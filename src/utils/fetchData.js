import axios from "axios"
export const fetchData=(url)=>{
    let responseData = axios.get(url).then(function(res){
        return res.data
    }).catch(function(error){
        console.log(error)
    })
return responseData;
}

export const findById=(url)=>{
    let responseData = axios.get(url).then(function(res){
        return res.data
    }).catch(function(error){
        console.log(error)
    })
return responseData;
}

export const postData=(url,title,authorName)=>{
    let data = {title:title,authorName:authorName};
    let responseData = axios.post(url,data).then(function(res){
        return res.data
    }).catch(function(error){
        console.log(error)
    })
return responseData;
}

export const fetchDeleteBook=(url)=>{
    
    let deleteData = axios.delete(url).then(function(res){
        return res.data
    }).catch(function(error){
        console.log(error)
    })
return deleteData;
}

export const updatedata=(url,title,authorName,id)=>{
    let data = {id:id,title:title,authorName:authorName}
    let updateData = axios.put(url,data).then(function(res){
        return res.data
    }).catch(function(error){
        console.log(error)
    })
return updateData;

}

