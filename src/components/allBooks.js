import React, { useEffect, useState } from 'react';
import SearchBook from './searchBook';
import './style.css';
import {fetchData,fetchDeleteBook,findById,postData,updatedata} from '../utils/fetchData'
import BookInfo from './bookInfo';
import { FaBeer } from 'react-icons/fa';


function AllBooks() {
const [allBooks,setAllBooks] = useState([])
const [addBook,setAddBook] = useState(false)
const[updateBook,setUpdateBook] = useState(false)
const [updateId,setUpdateBookId] = useState()

useEffect(()=>{
    let url = 'http://localhost:8086/allbooks'; 
    async function fetchMyAPI() {
        let res =  await fetchData(url)
        setAllBooks(res)
       
    }
    fetchMyAPI()
    
},[])
const onSearch=async(e)=>{

    if(e.key === 'Enter'){
        let id = e.target.value
        if(id.length!==0){
    let url = `http://localhost:8086/singleBook/${id}`
    let res =  await findById(url)
    let arr = [];
    arr.push(res)
        setAllBooks(arr)
    }
    else {
        getData()
    }
}
      
}
const deleteBook=async(data)=>{
    let id =data;
    let deletUrl=`http://localhost:8086/deleteBook/${id}`;
    await fetchDeleteBook(deletUrl)
    getData()
}
const onAddBook=async(title,authorName)=>{
    let url="http://localhost:8086/addBook";
    await postData(url,title,authorName)
    setAddBook(false)
    getData()
}
const onUpadateBook=async(title,authorName)=>{
    let url=`http://localhost:8086/updateBook`;
    await updatedata(url,title,authorName,updateId)
    getData()
    setUpdateBook(false)
}
const getData=async()=>{
    let url = 'http://localhost:8086/allbooks'; 
          let res =  await fetchData(url)
          setAllBooks(res)
}
const onUpdateButton=(e)=>{
    setUpdateBook(true)
    setUpdateBookId(parseInt(e.target.id))
}
const onBack=()=>{
setAddBook(false)
setUpdateBook(false)
}
        return (
            <div className='main-section'>
                <div className='header'>
                <h1>Book App</h1>
                </div>
               { addBook && <h6 onClick={onBack} className='back'>Back</h6>}
                <div className="headerSection">
                <SearchBook  onSearch={onSearch}/>
                { addBook === false &&
                <button onClick={()=>{setAddBook(true)}} className="addButton">Add book +</button>
                }
                 </div>
                { addBook &&
                <BookInfo onBookInfo={onAddBook} title="Add Book"/>
                }
             
                    {
                        allBooks.length === 0 &&
                        <span className="loading">Loading...</span>
                    }
                    {
                        updateBook &&
                        <BookInfo onBookInfo={onUpadateBook} title="Update Book"/>
                    }
                
                <div className="totalBooks">
                {allBooks.length > 0 && !addBook && !updateBook && 
                    allBooks.map((eachBook)=>{
                        return <div className="eachBookDiv" key={eachBook.id}>
                            <h1>{eachBook.title}</h1>
                            <p>{eachBook.authorName}</p>
                            <div className="bookFooter">
                            <p id={eachBook.id} onClick={(e) => deleteBook(e.target.id)} ><FaBeer id={eachBook.id} onClick={(e) => deleteBook(e.target.id)} /></p>
                            <p onClick={onUpdateButton} id={eachBook.id}>Update</p>
                            </div>
                        </div>

                    })
                    
                }
                </div>
            </div>
        );
    
}

export default AllBooks;