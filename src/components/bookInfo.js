import { useState } from "react";
function BookInfo(props){
const [bookName,setBookName] = useState('')
const [authorName,setAuthorName]=useState('')
const onSubmit=(e)=>{
    e.preventDefault()
    if(bookName.length!==0 && authorName!==0){
        props.onBookInfo(bookName,authorName)
    
    }
    else {
        alert('enter the value')
    }
   
}
    return(
        <form className="bookInfo">
            <div className="book-section">
            <h1 className="title">{props.title}</h1>
                    <p>Book Title</p>
                    <input onChange={(e)=>{setBookName(e.target.value)}} className="input-ele"/>
                    <p>Author Name </p>
                    <input onChange={(e)=>{setAuthorName(e.target.value)}} className="input-ele" />
                    <div className="button-section">
                    <button className="button-ele" onClick={onSubmit} >submit</button>
                    </div>
                    </div>
        </form>
    )
}

export default BookInfo;